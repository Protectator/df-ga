---
layout: page
title: Shop
permalink: /shop/
---

This is an example page that could show a list of items to buy.

<ul>
{% for item in site.items %}
    <li>
        <a href="{{site.baseurl}}{{item.url}}.html">{{item.title}}</a>
    </li>
{% endfor %}
</ul>