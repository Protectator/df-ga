---
layout: page
title: About
permalink: /about/
---

This site shows placeholder content and its sole purpose is showing the possibilities of [Google Analytics](https://www.google.com/analytics/).

It is part of a Master's thesis project. To know more, visit the <a href='#'>project's page</a>.